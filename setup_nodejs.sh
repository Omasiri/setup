#!/bin/bash

#Install git core for version management
sudo apt-get -y install git-core

#install curl

sudo apt-get -y install curl

#install node version manager
sudo apt-get -y install nvm

curl https://raw.github.com/creationix/nvm/master/install.sh | sh

nvm install 0.10

#install heroku toolbelt
wget -qO- https://toolbelt.heroku.com/install-ubuntu.sh | sh


