#!/bin/bash

cd ~

echo "installing git"

sudo apt-get -y install git

#echo "installing mysql server 5.6"
sudo apt-get -y install mysql-server-5.6
echo "complete"


git config --global user.name 'Omasiri'
git config -- global user.email 'Omasiri@novadge.com'

sudo apt-get -y install unzip
echo "configured git, installed unzip"

echo "downloading tomcat"
wget http://mirror.cogentco.com/pub/apache/tomcat/tomcat-8/v8.0.9/bin/apache-tomcat-8.0.9.zip ~/


sudo unzip apache-tomcat-8.0.9.zip
sudo  mkdir -p /usr/opt/tomcat

sudo mv apache-tomcat-8.0.9 /usr/opt/tomcat/apache-tomcat-8.0.9

echo "downloaded tomcat"
echo "downloading java"
sudo wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u5-b13/jdk-8u5-linux-x64.tar.gz


sudo mkdir -p /usr/opt/java
# unzip java
sudo tar -xvzf jdk-8u5-linux-x64.tar.gz
# move to java directory
sudo mv jdk1.8.0_05 /usr/opt/java/jdk1.8.0_05

# set java home
JAVA_HOME=/usr/opt/java/jdk1.8.0_05
# add to .bashrc
sudo echo "export JAVA_HOME=$JAVA_HOME" >> .bashrc

sudo echo "export PATH=$PATH:$JAVA_HOME/bin" >> ~/.bashrc

echo "bashrc updated"
cd ~

curl -s get.gvmtool.net | bash

gvm install grails
gvm install groovy


